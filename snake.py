class Player:
    #player coordinates
    x=[]
    y=[]
    direction = 1
    size = 1
    score = 0

    def __init__(self,size):
        #the last pease removes the simbol for the snake
        self.size = size + 1
        #init the first coordinates
        for i in range(0,size+1):
            self.x.append(10)
            self.y.append(10)

    def update(self):
        #move the body
        for i in range(self.size-1,0,-1):
            self.x[i] = self.x[i-1]
            self.y[i] = self.y[i-1]
        # move the head
        if self.direction == 0:
            self.y[0] = self.y[0] - 1
        if self.direction == 1:
            self.y[0] = self.y[0] + 1
        if self.direction == 2:
            self.x[0] = self.x[0] - 1
        if self.direction == 3:
            self.x[0] = self.x[0] + 1
        #boundary conditions
        self.boundaries()

    def boundaries(self):
        #set the boundary conditions
        if(self.x[0]==0):self.x[0]=18
        if(self.x[0]==19):self.x[0]=1
        if(self.y[0]==0):self.y[0]=58
        if(self.y[0]==59):self.y[0]=1

    def mvLeft(self):
        if self.direction != 1:  self.direction = 0

    def mvRight(self):
        if self.direction != 0: self.direction = 1

    def mvUp(self):
        if self.direction != 3: self.direction = 2

    def mvDown(self):
        if self.direction != 2: self.direction = 3

    def grow(self):
        #growing
        self.size=self.size+1
        self.x.append(self.x[0])
        self.y.append(self.y[0])

class Food:
    from random import randint
    x=15
    y=15

    def newFood(self):
        #generate new food
        self.x=self.randint(2,18)
        self.y=self.randint(2,58)

class App:
    import curses
    from random import randint
    #Initializing curses
    stdscr=curses.initscr()
    curses.noecho()
    stdscr.notimeout(True)
    stdscr.keypad(True)
    curses.curs_set(0)
    #window
    win = curses.newwin(20, 60, 0, 0)
    #create player & food
    player = 0
    food = 0

    def collison(self,x,y):
        for i in range(self.player.size-1,0,-1):
            if (x == self.player.x[i] and y == self.player.y[i]):
                return 1
        return 0


    def __init__(self):
        logo ="""
         _____             _
        /  ___|           | |
        \\ `--. _ __   __ _| | _____
         `--. \\ '_ \\ / _` | |/ / _ \\
        /\\__/ / | | | (_| |   <  __/
        \\____/|_| |_|\\__,_|_|\\_\\___|


"""
        self.win.addstr(0, 27, logo)
        self.win.addstr(10, 13, 'Pres p to pause, q to quit')
        self.win.addstr(12, 13, 'Use awsd to move')
        self.win.addstr(14, 13, 'Pres ENTER to continue')
        self.win.getch()
        self.curses.halfdelay(1)
        self.win.clear()
        #create player and food objects
        self.player = Player(5)
        self.food = Food()

    def exit(self):
        #on exit
        self.stdscr.clear()
        self.curses.nocbreak()
        self.curses.echo()
        self.stdscr.keypad(0)
        self.curses.endwin()

    def drawWin(self):
        #draw the window
        self.win.border(0)
        self.win.getch()
        self.win.addstr(0, 25, ' Score ' + str(self.player.score) )


    def drawSnake(self):
        index=self.player.size-1
        for i in range(0,index):
            self.win.addstr(self.player.x[i],self.player.y[i], '*')
        self.win.addstr(self.player.x[index],self.player.y[index],' ')

    def drawFood(self):
        self.win.addstr(self.food.x,self.food.y, '@')

    def eat(self):

        if (self.player.x[0] == self.food.x and self.player.y[0] == self.food.y):
            self.win.addstr(self.food.x,self.food.y,' ')
            self.player.grow()
            self.food.newFood()
            self.player.score = self.player.score +1
            self.win.addstr(0, 25, ' Score ' + str(self.player.score) )

            while(self.collison(self.food.x,self.food.y) != 0):
                self.win.addstr(self.food.x,self.food.y,' ')
                self.food.newFood()

    def pause(self):
        pause ="""
            ______
            | ___ \\
            | |_/ /_ _ _   _ ___  ___
            |  __/ _` | | | / __|/ _ \\
            | | | (_| | |_| \__ \\  __/
            \_|  \__,_|\__,_|___/\___|

"""
        self.win.clear()
        self.curses.nocbreak()
        self.win.addstr(0, 27, pause)
        self.win.addstr(10, 13, 'Pres ENTER to continue')
        self.win.getch()
        self.curses.halfdelay(1)
        self.win.clear()
        self.drawWin()

    def lost(self):
        bye = """



  __ _  __ _ _ __ ___   ___    _____   _____ _ __
 / _` |/ _` | '_ ` _ \ / _ \  / _ \ \ / / _ \ '__|
| (_| | (_| | | | | | |  __/ | (_) \ V /  __/ |
 \__, |\__,_|_| |_| |_|\___|  \___/ \_/ \___|_|
  __/ |
 |___/

        """
        self.win.clear()
        self.curses.nocbreak()
        self.win.addstr(0, 27, bye)
        self.win.addstr(14, 19, 'Score is ' + str(self.player.score) + ' points')
        self.win.addstr(17, 19, 'Pres ENTER to exit')
        self.win.getch()
        self.exit()

    def on_execute(self):
        key = ord('d')
        self.drawWin()

        while( 1 ):

            prevKey = key              # Previous key pressed
            event = self.win.getch()
            key = key if event == -1 else event

            if ( key == ord('d')):
                self.player.mvRight()

            if (key == ord('a')):
                self.player.mvLeft()

            if (key == ord('w')):
                self.player.mvUp()

            if (key == ord('s')):
                self.player.mvDown()

            if (key == ord('p')):
                self.pause()
                key = ord(' ')

            if (key == ord('q')):
                self.lost()
                break

            self.player.update()

            if(self.collison(self.player.x[0],self.player.y[0])):
                self.lost()
                break


            self.eat()
            self.drawSnake()
            self.drawFood()

    def on_event(KeyboardInterrupt):
        self.curses.endwin()

def main():
    theApp = App()
    theApp.on_execute()

if __name__ == "__main__":
    main()
