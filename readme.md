# Snake game 

This is a simple [snake](https://en.wikipedia.org/wiki/Snake_(video_game_genre) "wikipedia") game using [ncurses](https://www.gnu.org/software/ncurses/ncurses.html "official site")


## Screenshots

![](/img/Start.png)
![](/img/Game.png)